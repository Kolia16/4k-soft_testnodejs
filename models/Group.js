const { Schema, model } = require("mongoose");

const groupSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
});

module.exports = model("groups", groupSchema);
