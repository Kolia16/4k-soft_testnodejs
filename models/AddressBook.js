const { Schema, model } = require("mongoose");

const addressBookSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  patronymic: {
    type: String,
    required: true,
  },
  surname: {
    type: String,
    required: true,
  },
  birthday: {
    type: Date,
    required: true,
  },
  imageSrc: {
    type: String,
    default: "",
  },
  phone: {
    type: String,
    required: true,
  },
  group: {
    ref: "groups",
    type: Schema.Types.ObjectId,
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  date_create: {
    type: Date,
    default: Date.now,
  },
  create_user: {
    ref: "users",
    type: Schema.Types.ObjectId,
    required: true,
  },
});

module.exports = model("address_books", addressBookSchema);
