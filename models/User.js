const { Schema, model } = require("mongoose");

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  patronymic: {
    type: String,
    required: true,
  },
  surname: {
    type: String,
    required: true,
  },
  date_register: {
    type: Date,
    default: Date.now,
  },
});

userSchema.virtual("fullName").get(function () {
  return this.surname + " " + this.name + " " + this.patronymic;
});

module.exports = model("users", userSchema);
