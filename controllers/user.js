const User = require("../models/User");

const errorHandler = require("../utils/errorHandler");

module.exports.getProfil = async (req, res) => {
  try {
    const user = await User.findById(req.user._id).select('email name surname patronymic date_register')
    res.status(200).json(user)
  } catch (e) {
    errorHandler(res, e);
  }
};
