const Group = require("../models/Group");
const AddressBook = require("../models/AddressBook");

const errorHandler = require("../utils/errorHandler");

module.exports.getAll = async (req, res) => {
  try {
    const groups = await Group.find();

    res.status(200).json(groups);
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.create = async (req, res) => {
  if (await Group.findOne({ name: req.body.name })) {
    res.status(409).json({ message: "Така група вже існує!" });
  } else {
    const group = await new Group(req.body).save();
    res.status(201).json(group);
  }
};

module.exports.remove = async (req, res) => {
  if (await AddressBook.findOne({ group: req.params.id })) {
    res.status(409).json({
      message: "Дану групу не можна видалити, так як вона, міснить записи!",
    });
  } else {
    await Group.remove({ _id: req.params.id });
    res.status(200).json({
      message: "Група успiшно видалений!",
    });
  }
  res.status(200).end();
};
