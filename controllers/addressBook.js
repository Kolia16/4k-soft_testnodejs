const fs = require("fs");
const path = require("path");
const validator = require("validator");

const AddressBook = require("../models/AddressBook");
const User = require("../models/User");
const Group = require("../models/Group");

const errorHandler = require("../utils/errorHandler");

module.exports.getAll = async (req, res) => {
  try {
    const addressBooks = await AddressBook.find().select(
      "name phone group _id"
    );
    res.status(200).json(addressBooks);
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.getById = async (req, res) => {
  try {
    const addressBook = await AddressBook.findById(req.params.id);

    if (addressBook) {
      const user = await User.findById(addressBook.create_user);
      const group = await Group.findById(addressBook.group);

      let record = {
        name_user: user.fullName,
        name_group: group.name,
        name: addressBook.name,
        surname: addressBook.surname,
        patronymic: addressBook.patronymic,
        birthday: addressBook.birthday,
        imageSrc: addressBook.imageSrc,
        phone: addressBook.phone,
        city: addressBook.city,
        group: addressBook.group,
        date_create: addressBook.date_create,
        _id: addressBook._id,
      };

      res.status(200).json(record);
    } else {
      res.status(404).json({ message: "Такий запис не знайдено!" });
    }
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.create = async (req, res) => {
  try {
    const birthday = await new Date(req.body.birthday);
    const date = await new Date();

    if (!validator.isMobilePhone(req.body.phone, ["uk-UA"])) {
      res.status(409).json({ message: "Не коректний номер телефону!" });
    } else if (birthday > date) {
      res.status(409).json({ message: "Така дата народження ще не настала!" });
    } else {
      const record = await new AddressBook({
        name: req.body.name,
        patronymic: req.body.patronymic,
        surname: req.body.surname,
        birthday: req.body.birthday,
        imageSrc: req.file ? req.file.path : "",
        phone: req.body.phone,
        group: req.body.group,
        city: req.body.city,
        create_user: req.user._id,
      }).save();

      res.status(201).json({ record });
    }
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.update = async (req, res) => {
  try {
    const birthday = await new Date(req.body.birthday);
    const date = await new Date();

    if (!validator.isMobilePhone(req.body.phone, ["uk-UA"])) {
      res.status(409).json({ message: "Не коректний номер телефону!" });
    } else if (birthday > date) {
      res.status(409).json({ message: "Така дата народження ще не настала!" });
    } else {
      const updated = {
        ...req.body,
      };

      if (req.file) {
        updated.imageSrc = req.file.path;
      }

      const record = await AddressBook.findOneAndUpdate(
        { _id: req.params.id },
        { $set: updated },
        { new: true }
      );

      res.status(201).json({ record });
    }
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.remove = async (req, res) => {
  try {
    const record = await AddressBook.findById(req.params.id);

    if (req.body.imageSrc) {
      const dir = path.dirname(__dirname);
      const dirPath = path.join(dir, record.imageSrc);

      await fs.stat(dirPath, function (err) {
        if (!err) {
          fs.unlinkSync(dirPath);
        }
      });
    }

    await record.remove();

    res.status(200).json({
      message: "Запис успiшно видалений!",
    });
  } catch (e) {
    errorHandler(res, e);
  }
};
