const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const validator = require("validator");

const keys = require("../config/keys");

const User = require("../models/User");

const errorHandler = require("../utils/errorHandler");

module.exports.login = async (req, res) => {
  try {
    if (!validator.isEmail(req.body.email)) {
      res.status(409).json({ message: "Некоректна пошта!" });
    } else if (
      !validator.isLength(req.body.password, { min: 6, max: undefined })
    ) {
      res
        .status(409)
        .json({ message: "Пароль повинен мати мініум 6 символів!" });
    } else {
      const candidate = await User.findOne({ email: req.body.email });
      if (candidate) {
        const passwordResult = bcrypt.compareSync(
          req.body.password,
          candidate.password
        );

        if (passwordResult) {
          const token = jwt.sign(
            {
              email: candidate.email,
              userId: candidate._id,
            },
            keys.jwt,
            { expiresIn: 60 * 60 }
          );

          res.status(200).json({
            token: `Bearer ${token}`,
          });
        } else {
          res.status(401).json({ message: "Паролі не співпадають!" });
        }
      } else {
        res.status(404).json({
          message: "Користувач із таким email, не знайден!",
        });
      }
    }
  } catch (e) {
    errorHandler(res, e);
  }
};

module.exports.register = async (req, res) => {
  try {
    const candidate = await User.findOne({ email: req.body.email });

    if (!validator.isEmail(req.body.email)) {
      res.status(409).json({ message: "Некоректна пошта!" });
    } else if (
      !validator.isLength(req.body.password, { min: 6, max: undefined })
    ) {
      res
        .status(409)
        .json({ message: "Пароль повинен мати мініум 6 символів!" });
    } else {
      if (candidate) {
        res.status(409).json({ message: "Такий email вже зайнятий!" });
      } else {
        const salt = bcrypt.genSaltSync(10);
        const password = req.body.password;
        const user = await new User({
          email: req.body.email,
          password: bcrypt.hashSync(password, salt),
          name: req.body.name,
          patronymic: req.body.patronymic,
          surname: req.body.surname,
        }).save();

        res.status(201).json(user);
      }
    }
  } catch (e) {
    errorHandler(res, e);
  }
};
