const express = require("express");
const passport = require("passport");

const controller = require("../controllers/user");

const router = express.Router();

router.get(
  "/profil",
  passport.authenticate("jwt", { session: false }),
  controller.getProfil
);

module.exports = router;
