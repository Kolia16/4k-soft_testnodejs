import {
  MaterialInstanse,
  MaterialService,
} from './../../shared/classes/material.service';
import { GroupService } from './../../shared/services/group.service';
import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-group-page',
  templateUrl: './group-page.component.html',
  styleUrls: ['./group-page.component.scss'],
})
export class GroupPageComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('modal') modalRef: ElementRef;
  modal: MaterialInstanse;

  loading: boolean;

  gSub: Subscription;

  idGroupRemove: string;
  nameGroupRemove: string;

  get groups() {
    return this.groupService.groups;
  }

  constructor(private groupService: GroupService) {}

  ngOnInit(): void {
    this.loading = true;

    this.gSub = this.groupService.getAll().subscribe((groups) => {
      this.groupService.groups = groups;
      this.loading = false;
    });
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  ngOnDestroy() {
    this.gSub.unsubscribe();
    this.modal.destroy()
  }

  removeGroup(id, name) {
    this.idGroupRemove = id;
    this.nameGroupRemove = name;
    this.modal.open();
  }

  remove() {
    this.loading = true;

    this.groupService.remove(this.idGroupRemove).subscribe(
      (res) => {
        const idx = this.groupService.groups.findIndex(
          (x) => x._id === this.idGroupRemove
        );
        this.groupService.groups.splice(idx, 1);

        MaterialService.toast(res.message);

        this.idGroupRemove = '';
        this.nameGroupRemove = '';

        this.loading = false;
      },
      (error) => {
        MaterialService.toast(error.error.message)
        this.loading = false
      }
    );
  }
}
