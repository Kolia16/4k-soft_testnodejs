import { User } from './../../shared/interfaces';
import { UserService } from './../../shared/services/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profil-page',
  templateUrl: './profil-page.component.html',
  styleUrls: ['./profil-page.component.scss'],
})
export class ProfilPageComponent implements OnInit, OnDestroy {

  loading: boolean

  uSub: Subscription

  user: User

  get dateRegister() {
    let date = new Date(this.user.date_register)
    return `${date.getDate()}.${date.getMonth()}.${date.getFullYear()}`
  }

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.loading = true

    this.uSub = this.userService.getProfil().subscribe(data=>{
      this.user = data
      this.loading = false
    })
  }

  ngOnDestroy() {
    this.uSub.unsubscribe()
  }
}
