import { Record } from './../../shared/interfaces';
import { AddressBookService } from './../../shared/services/address-book.service';
import { MaterialService } from './../../shared/classes/material.service';
import { GroupService } from './../../shared/services/group.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { of, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-address-book-create-and-update-page',
  templateUrl: './address-book-create-and-update-page.component.html',
  styleUrls: ['./address-book-create-and-update-page.component.scss'],
})
export class AddressBookCreateAndUpdatePageComponent
  implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('inputImage') inputImageRef: ElementRef;
  @ViewChild('birthday') birthdayRef: ElementRef;
  birthday;

  isNew: boolean = true;

  form: FormGroup;

  record: Record;

  image: File;
  imagePreview;

  rSub: Subscription;
  gSub: Subscription;

  get groups() {
    return this.groupService.groups;
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private groupService: GroupService,
    private addressBookService: AddressBookService
  ) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      patronymic: new FormControl('', Validators.required),
      surname: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      group: new FormControl('', Validators.required),
      birthday: new FormControl('', Validators.required),
    });

    this.form.disable();

    this.gSub = this.groupService.getAll().subscribe((groups) => {
      this.groupService.groups = groups;
    });

    this.rSub = this.route.params
      .pipe(
        switchMap((params: Params) => {
          if (params['id']) {
            this.isNew = false;

            return this.addressBookService.getById(params['id']);
          }
          return of(null);
        })
      )
      .subscribe(
        (record: Record) => {
          if (record) {
            this.record = record;
            const date = new Date(record.birthday);
            this.form.patchValue({
              name: record.name,
              surname: record.surname,
              patronymic: record.patronymic,
              phone: record.phone,
              city: record.city,
              group: record.group,
              birthday: `${date.getDate()}/${
                date.getMonth() + 1
              }/${date.getFullYear()}`,
            });
            this.imagePreview = record.imageSrc;
            MaterialService.updateTextInputs();
          }
          this.form.enable();
        },
        (error) => {
          MaterialService.toast(error.error.message);
        }
      );
  }

  ngAfterViewInit() {
    this.birthday = MaterialService.iniDatepicker(this.birthdayRef, (date) => {
      this.form.get('birthday').patchValue(date);
    });
  }

  ngOnDestroy() {
    if (this.rSub) this.rSub.unsubscribe();
    if (this.gSub) this.gSub.unsubscribe();
  }

  triggerClick() {
    this.inputImageRef.nativeElement.click();
  }

  onFileUpload(event: any) {
    const file = event.target.files[0];
    this.image = file;

    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result || '';
    };
    reader.readAsDataURL(file);
  }

  onSubmit() {
    this.form.disable();

    let obs$;

    if (this.isNew) {
      obs$ = this.addressBookService.create(this.form.value, this.image);
    } else {
      obs$ = this.addressBookService.update(
        this.record._id,
        this.form.value,
        this.image
      );
    }

    obs$.subscribe(
      (record: Record) => {
        if (this.isNew) {
          this.addressBookService.records.unshift(record);
          MaterialService.toast('Створено новий запис!')
        } else {
          console.log(record);

          let idx = this.addressBookService.records.findIndex(
            (x) => x._id == this.record._id
          );

          Object.assign(this.addressBookService.records[idx], record);
          MaterialService.toast('Оновлено запис!')
        }
      },
      (error) => {
        MaterialService.toast(error.error.message);
        this.form.enable();
      },
      () => {
        this.router.navigate(['/main/address-book']);
        this.form.enable();
      }
    );
  }

}
