import { GroupService } from './../../shared/services/group.service';
import {
  MaterialService,
  MaterialInstanse,
} from './../../shared/classes/material.service';
import { AddressBookService } from './../../shared/services/address-book.service';
import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  OnDestroy,
  AfterViewInit,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-address-book-page',
  templateUrl: './address-book-page.component.html',
  styleUrls: ['./address-book-page.component.scss'],
})
export class AddressBookPageComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('modal') modalRef: ElementRef;
  modal: MaterialInstanse;

  loading: boolean;

  searchName: string = '';
  searchPhone: string = '';
  searchNameGroup: string = '';

  aSub: Subscription;
  gSub: Subscription;

  idAddressBookRemove: string;
  nameAddressBookRemove: string;

  get records() {
    return this.addressBookService.records;
  }

  constructor(
    private addressBookService: AddressBookService,
    private groupService: GroupService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loading = true;

    this.aSub = this.addressBookService.getAll().subscribe((records) => {
      this.gSub = this.groupService.getAll().subscribe((groups) => {
        const arrRecords = records.map((record) => {
          const name_group = groups.find((group) => group._id == record.group)
            .name;
          return {
            ...record,
            name_group,
          };
        });

        this.addressBookService.records = arrRecords;
        this.loading = false;
      });
    });
  }

  ngAfterViewInit(): void {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  ngOnDestroy() {
    this.aSub.unsubscribe();
    if (this.gSub) this.gSub.unsubscribe();
  }

  onRouterRecord(id) {
    this.router.navigate(['/main/record', id]);
  }

  removeAddressBook(id, name) {
    this.idAddressBookRemove = id;
    this.nameAddressBookRemove = name;
    this.modal.open();
  }

  remove() {
    this.loading = true;

    this.addressBookService.remove(this.idAddressBookRemove).subscribe(
      (res) => {
        const idx = this.addressBookService.records.findIndex(
          (x) => x._id === this.idAddressBookRemove
        );
        this.addressBookService.records.splice(idx, 1);

        MaterialService.toast(res.message);

        this.idAddressBookRemove = '';
        this.nameAddressBookRemove = '';

        this.loading = false;
      },
      (error) => {
        MaterialService.toast(error.error.message);
        this.loading = false;
      }
    );
  }

  sort(status: boolean = true) {
    this.addressBookService.records.sort((a, b) => (a.name > b.name ? 1 : -1));
    if(!status)this.addressBookService.records.reverse()
  }
}
