import { MaterialService } from './../../shared/classes/material.service';
import { GroupService } from './../../shared/services/group.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Group } from './../../shared/interfaces';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-group-create-and-update-page',
  templateUrl: './group-create-and-update-page.component.html',
  styleUrls: ['./group-create-and-update-page.component.scss'],
})
export class GroupCreateAndUpdatePageComponent implements OnInit, OnDestroy {
  form: FormGroup;

  constructor(private groupService: GroupService, private router: Router) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required)
    })
  }

  ngOnDestroy() {}

  onSubmit() {
    this.form.disable()

    this.groupService.create(this.form.value).subscribe(
      group=>{
        this.groupService.groups.unshift(group)
        this.router.navigate(['/main/group'])
      },
      error=>{
        MaterialService.toast(error.error.message)
        this.form.enable()
      }
    )
  }

}
