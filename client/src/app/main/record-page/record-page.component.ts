import { MaterialService } from './../../shared/classes/material.service';
import { Record } from './../../shared/interfaces';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AddressBookService } from './../../shared/services/address-book.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-record-page',
  templateUrl: './record-page.component.html',
  styleUrls: ['./record-page.component.scss'],
})
export class RecordPageComponent implements OnInit, OnDestroy {
  loading: boolean;

  rSub: Subscription;
  aSub: Subscription;

  record: Record;

  get birthday() {
    let date = new Date(this.record?.birthday)
    return `${date.getDate()}.${date.getMonth()}.${date.getFullYear()}`
  }

  get dateCreatedRecord() {
    let date = new Date(this.record?.date_create)
    return `${date.getDate()}.${date.getMonth()}.${date.getFullYear()}`
  }

  constructor(
    private addressBookService: AddressBookService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loading = true;

    this.rSub = this.route.params.subscribe((params) => {
      this.aSub = this.addressBookService
        .getById(params.id)
        .subscribe((record: Record) => {
          console.log(record);
          this.record = record;
          this.loading = false;

        },
        error=>{
          MaterialService.toast(error.error.message)
          this.router.navigate(['/main/address-book'])
        });
    });
  }

  ngOnDestroy() {}
}
