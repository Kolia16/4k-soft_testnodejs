import { RecordPageComponent } from './main/record-page/record-page.component';
import { AuthGuard } from './shared/classes/auth.guard';
import { ErrorPageComponent } from './error-page/error-page.component';
import { ProfilPageComponent } from './main/profil-page/profil-page.component';
import { AddressBookCreateAndUpdatePageComponent } from './main/address-book-create-and-update-page/address-book-create-and-update-page.component';
import { AddressBookPageComponent } from './main/address-book-page/address-book-page.component';
import { GroupCreateAndUpdatePageComponent } from './main/group-create-and-update-page/group-create-and-update-page.component';
import { GroupPageComponent } from './main/group-page/group-page.component';
import { MainLayoutComponent } from './shared/layouts/main-layout/main-layout.component';
import { RegisterPageComponent } from './auth/register-page/register-page.component';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full',
  },
  {
    path: 'main',
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'profil',
        pathMatch: 'full',
      },
      {
        path: 'profil',
        component: ProfilPageComponent,
      },

      {
        path: 'group',
        component: GroupPageComponent,
      },
      {
        path: 'group/new',
        component: GroupCreateAndUpdatePageComponent,
      },

      {
        path: 'address-book',
        component: AddressBookPageComponent,
      },
      {
        path: 'address-book/new',
        component: AddressBookCreateAndUpdatePageComponent,
      },
      {
        path: 'address-book/:id',
        component: AddressBookCreateAndUpdatePageComponent,
      },
      {
        path: 'record/:id',
        component: RecordPageComponent,
      },
    ],
  },
  {
    path: 'auth',
    component: AuthLayoutComponent,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginPageComponent },
      { path: 'register', component: RegisterPageComponent },
    ],
  },
  { path: 'error', component: ErrorPageComponent },
  { path: '**', redirectTo: '/error' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
