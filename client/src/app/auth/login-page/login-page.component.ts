import { MaterialService } from './../../shared/classes/material.service';
import { AuthService } from './../../shared/services/auth.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit, OnDestroy {
  form: FormGroup;

  aSub: Subscription;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
      ]),
    });

    this.route.queryParams.subscribe(
      (params: Params)=>{
        if(params['registered']) {
          MaterialService.toast('Тепер ви можете зайти в систему використовуючи свої дані!')
        }else if(params['accessDenied']) {
          MaterialService.toast('Для початку авторизуйтесь в системі!')
        }else if(params['sessionFailed']) {
          MaterialService.toast('Перезайдіть будь ласка в систему!')
        }
      }
    )
  }
  ngOnDestroy() {
    if (this.aSub) this.aSub.unsubscribe();
  }

  onSubmit() {
    this.form.disable();
    this.aSub = this.authService.login(this.form.value).subscribe(
      (token) => {
        this.router.navigate(['/main'])
        this.form.enable();
      },
      (error) => {
        MaterialService.toast(error.error.message)
        this.form.enable();
      }
    );
  }
}
