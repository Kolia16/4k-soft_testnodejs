import { Record } from './../interfaces';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterAddressBook',
})
export class FilterAddressBookPipe implements PipeTransform {
  transform(value: Record[], name, phone, nameGroup): Record[] {
    if (!name.trim() && !phone.trim() && !nameGroup.trim()) {
      return value;
    }

    let filterData: Array<Record> = value;

    filterData = value.filter((record) => {
      if (record.name.includes(name)) {
        return true;
      }
      return false;
    });

    filterData = filterData.filter((record) => {
      if (record.phone.includes(phone)) {
        return true;
      }
      return false;
    });

    filterData = filterData.filter((record) => {
      if (record.name_group.includes(nameGroup)) {
        return true;
      }
      return false;
    });

    return filterData;
  }
}
