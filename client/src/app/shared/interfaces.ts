export interface User {
  name?: string;
  surname?: string;
  patronymic?: string;
  date_register?: string;
  email?: string;
  password?: string;
}

export interface Group {
  name: string;
  _id: string;
}

export interface Record {
  name?: string;
  surname?: string;
  patronymic?: string;
  birthday?: Date | string
  imageSrc?: string;
  phone?: string;
  city?: string;
  group?: string;
  name_group?: string;
  date_create?: Date;
  create_user?: string;
  name_user?: string;
  _id?: string
}
