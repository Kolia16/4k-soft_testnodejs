import { ElementRef } from '@angular/core';

declare var M;

export interface MaterialInstanse {
  open?(): void;
  close?(): void;
  destroy?(): void;
}

export class MaterialService {
  static updateTextInputs() {
    M.updateTextFields();
  }

  static toast(message: string) {
    M.toast({ html: message });
  }

  static initSidenav(ref: ElementRef) {
    return M.Sidenav.init(ref.nativeElement);
  }

  static initFormSelect(ref: ElementRef) {
    return M.FormSelect.init(ref.nativeElement);
  }

  static initTooltip(ref: ElementRef) {
    return M.Tooltip.init(ref.nativeElement);
  }

  static initModal(ref: ElementRef) {
    return M.Modal.init(ref.nativeElement);
  }

  static initCollapsible(ref: ElementRef) {
    return M.Collapsible.init(ref.nativeElement);
  }

  static initFloatingActionButton(ref: ElementRef) {
    return M.FloatingActionButton.init(ref.nativeElement);
  }

  static iniDatepicker(
    ref: ElementRef,
    formGet: Function = undefined
  ) {
    let dateEnd = new Date();


    const option = {
      onSelect: (date: Date) => {
        if (formGet) {
          formGet(
            `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
          );
          this.updateTextInputs()
        }
      },
      format: 'dd/mm/yyyy',
      maxDate: dateEnd,
      i18n: {
        cancel: '',
        weekdaysAbbrev: ['Н', 'П', 'В', 'С', 'Ч', 'П', 'С'],
        months: [
          'Січень',
          'Лютий',
          'Березень',
          'Квітень',
          'Травень',
          'Червень',
          'Липень',
          'Серпень',
          'Вересень',
          'Жовтень',
          'Листопад',
          'Грудень',
        ],
        monthsShort: [
          'Січень',
          'Лютий',
          'Березень',
          'Квітень',
          'Травень',
          'Червень',
          'Липень',
          'Серпень',
          'Вересень',
          'Жовтень',
          'Листопад',
          'Грудень',
        ],
        weekdays: [
          'Неділя',
          'Понеділок',
          'Вівторок',
          'Середа',
          'Четвер',
          'Пятниця',
          'Субота',
        ],
        weekdaysShort: [
          'Неділя',
          'Понеділок',
          'Вівторок',
          'Середа',
          'Четвер',
          'Пятниця',
          'Субота',
        ],
      },
    };
    return M.Datepicker.init(ref.nativeElement, option);
  }

}
