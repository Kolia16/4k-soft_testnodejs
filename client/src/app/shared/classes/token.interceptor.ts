import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { AuthService } from './../services/auth.service';
import { catchError } from 'rxjs/operators';

@Injectable()

export class TokenInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (this.auth.isAuthnticated()) {
      req = req.clone({
        setHeaders: {
          Authorization: this.auth.getToken()
        }
      })
    }

    return next.handle(req).pipe(
      catchError(
        (error: HttpErrorResponse) => this.handleAuthError(error)
      )
    )
  }

  private handleAuthError(error: HttpErrorResponse): Observable<any> {

    if (error.status === 401) {
      this.auth.logout()
      this.router.navigate(['/auth/login'], {
        queryParams: {
          sessionFailed: true
        }
      })
    } else if (error.status == 403) {
      this.auth.logout()
      this.router.navigate(['/auth/login'], {
        queryParams: {
          userDenied: true
        }
      })
    } else if (error.status === 500) {
      this.auth.logout()
      this.router.navigate(['/error'])
    }

    return throwError(error)
  }
}
