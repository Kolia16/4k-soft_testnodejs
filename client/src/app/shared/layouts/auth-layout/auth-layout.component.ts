import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.scss'],
})
export class AuthLayoutComponent implements OnInit {
  links= [
    { name: 'Вхів', url: 'login' },
    { name: 'Реєстрація', url: 'register' },
  ];

  constructor() {}

  ngOnInit(): void {}
}
