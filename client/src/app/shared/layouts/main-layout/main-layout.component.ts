import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit {
  links = [
    { name: 'Профіль', url: 'profil' },
    { name: 'Групи', url: 'group' },
    { name: 'Адресна книга', url: 'address-book' },
  ];

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {}

  logout(event: Event) {
    event.preventDefault();
    this.authService.logout()
    this.router.navigate(['/auth/login'])
  }
}
