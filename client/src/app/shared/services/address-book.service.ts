import { Record } from './../interfaces';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AddressBookService {
  records: Record[] = [];

  constructor(private http: HttpClient) {}

  getAll(): Observable<Record[]> {
    return this.http.get<Record[]>('/api/address-book');
  }

  getById(id: string): Observable<Record> {
    return this.http.get<Record>(`/api/address-book/${id}`);
  }

  create(record: Record, image?: File): Observable<Record> {
    const fd = new FormData();
    const date = (record.birthday as String).split('/');
    const dateBirthday = new Date(`${date[2]}.${date[1]}.${date[0]}`);

    if (image) {
      fd.append('image', image, image.name);
    }

    fd.append('name', record.name);
    fd.append('surname', record.surname);
    fd.append('patronymic', record.patronymic);
    fd.append('phone', record.phone);
    fd.append('city', record.city);
    fd.append('group', record.group);
    fd.append('birthday', dateBirthday.toJSON());

    return this.http.post<Record>(`/api/address-book`, fd);
  }

  update(id: string, record: Record, image?: File): Observable<Record> {
    const fd = new FormData();
    const date = (record.birthday as String).split('/');
    const dateBirthday = new Date(`${date[2]}.${date[1]}.${date[0]}`);

    if (image) {
      fd.append('image', image, image.name);
    }

    fd.append('name', record.name);
    fd.append('surname', record.surname);
    fd.append('patronymic', record.patronymic);
    fd.append('phone', record.phone);
    fd.append('city', record.city);
    fd.append('group', record.group);
    fd.append('birthday', dateBirthday.toJSON());

    return this.http.patch<Record>(`/api/address-book/${id}`, fd);
  }

  remove(id): Observable<{ message: string }> {
    return this.http.delete<{ message: string }>(`/api/address-book/${id}`);
  }


}
