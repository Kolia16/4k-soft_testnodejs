import { Group } from './../interfaces';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class GroupService {
  groups: Array<Group> = [];

  constructor(private http: HttpClient) {}

  getAll(): Observable<Group[]> {
    return this.http.get<Group[]>('/api/group');
  }

  remove(id): Observable<{ message: string }> {
    return this.http.delete<{ message: string }>(`/api/group/${id}`);
  }

  create(group): Observable<Group> {
    return this.http.post<Group>('/api/group', group);
  }
}
