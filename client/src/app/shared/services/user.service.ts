import { Observable } from 'rxjs';
import { User } from './../interfaces';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  constructor(private http: HttpClient) {}

  getProfil(): Observable<User> {
    return this.http.get<User>('/api/user/profil');
  }
}
