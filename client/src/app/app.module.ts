import { TokenInterceptor } from './shared/classes/token.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { RegisterPageComponent } from './auth/register-page/register-page.component';
import { AuthLayoutComponent } from './shared/layouts/auth-layout/auth-layout.component';
import { MainLayoutComponent } from './shared/layouts/main-layout/main-layout.component';
import { GroupPageComponent } from './main/group-page/group-page.component';
import { GroupCreateAndUpdatePageComponent } from './main/group-create-and-update-page/group-create-and-update-page.component';
import { AddressBookPageComponent } from './main/address-book-page/address-book-page.component';
import { AddressBookCreateAndUpdatePageComponent } from './main/address-book-create-and-update-page/address-book-create-and-update-page.component';
import { ProfilPageComponent } from './main/profil-page/profil-page.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { LoaderComponent } from './shared/components/loader/loader.component';
import { FilterAddressBookPipe } from './shared/pipes/filter-address-book.pipe';
import { CommonModule } from '@angular/common';
import { RecordPageComponent } from './main/record-page/record-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    RegisterPageComponent,
    AuthLayoutComponent,
    MainLayoutComponent,
    GroupPageComponent,
    GroupCreateAndUpdatePageComponent,
    AddressBookPageComponent,
    AddressBookCreateAndUpdatePageComponent,
    ProfilPageComponent,
    ErrorPageComponent,
    LoaderComponent,
    FilterAddressBookPipe,
    RecordPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: TokenInterceptor
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
