const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const passport = require("passport");

const authRoutes = require("./routers/auth");
const groupRoutes = require("./routers/group");
const addressBookRoutes = require("./routers/addressBook");
const userRoutes = require("./routers/user");

const { mongodbUri } = require("./config/keys");

const app = express();

mongoose
  .connect(mongodbUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,

    useCreateIndex: true,
  })
  .then(() => console.log("MongoDB connected"))
  .catch((error) => console.log(error));

app.use(passport.initialize()); 
require("./middleware/passport")(passport);

app.use(require("morgan")("dev"));

app.use('/uploads', express.static('uploads'))

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(require("cors")());

app.use("/api/auth", authRoutes);
app.use("/api/group", groupRoutes);
app.use("/api/address-book", addressBookRoutes);
app.use("/api/user", userRoutes);

module.exports = app;
