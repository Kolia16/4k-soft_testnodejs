const app = require('./app')

const port = process.env.PORT || 5000

try {

    app.listen(port, () => {
        console.log(`Good started server! PORT - ${port}`)
    })
} catch (e) {
    console.log(e)
}



